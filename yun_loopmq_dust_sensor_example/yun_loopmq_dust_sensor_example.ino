/*
 Litmus Loop
*/

/*
Inlcude all the library files 
*/
#include <loopmq.h>
#include <Bridge.h>
#include<BridgeClient.h>
#include <stdlib.h>
#include <stdio.h>
#include <ArduinoJson.h> 
#include "configuration.h"

/*
Passing the defined information to char
*/
int port = port_number;                       // Port number
char hostname[] = server;                     // Server name
char c[15] = clientID;                        // ClientID
char pass[16]= password;                      // password
char user[16] = userID;                       // username
char p[]= subTOPIC;                           // Subscribe on this topic to get the data
char s[]= pubTopic;                           // Publish on this topic to send data or command to device 

/* 
 constants won't change. They're used here to
 set pin numbers:
 */ 
int pin = 8;
unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 2000;//sampe 30s&nbsp;;
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;

BridgeClient yunClient;                       // yun client
PubSubClient loopmq(yunClient);               // instance to use the loopmq functions.

void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
  
/*
Display the message published serially
*/
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);           // Serial Display
  }
     Serial.println();                         //Print on a new line
}

void reconnect() {                        
   
  while (!loopmq.connected()) {               // Loop until we're reconnected
  Serial.print("Attempting MQTT connection...");    // Attempt to connect    
    if (loopmq.connect(c, user, pass)) {
    Serial.println("connected");            // Once connected, publish an announcement...

         loopmq.subscribe(s);                 // Subscribe to a topic  
/*  
To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   
      /*
To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   Serial.print("failed, rc=");
   Serial.print(loopmq.state());
   Serial.println(" try again in 5 seconds");
      
      delay(5000);                            // Wait 5 seconds before retrying
    }
  }
}
 
void setup() {
  /* 
   Bridge takes about two seconds to start up
   it can be helpful to use the on-board LED
   as an indicator for when it has initialized
  */
     
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);

  Serial.begin(57600);                //Start the Serial Connection
  
  pinMode(8,INPUT);
  starttime = millis();//get the current time;

  loopmq.setServer(hostname, port);           // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);               // Call the callbeck funciton when published    

  delay(1500);                                // Allow the hardware to sort itself out
}
 
void loop() {
  /*
   Sesnor code for dust sensor
   */
  duration = pulseIn(pin, LOW);
  lowpulseoccupancy = lowpulseoccupancy+duration;
 
  if ((millis()-starttime) >= sampletime_ms)//if the sampel time = = 30s
  {
    ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // Integer percentage 0=&gt;100
    concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
    Serial.print("concentration = ");
    Serial.print(concentration);
    Serial.println(" pcs/0.01cf");
    Serial.println("\n");
    lowpulseoccupancy = 0;
    starttime = millis();
  }

   /*  
  JSON parser
  */
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "Dust Sensor";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;
 // root["number"]= var;

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Dust concentration "]= concentration;                                // Add data["key"]= value
  data["units"]="pcs/0.01cf";


  root.printTo(Serial);                           // prints to serial terminal
  Serial.println();

  char buffer[100];                               // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           // copy the JSON to the buffer to pass as a payload 
  /*
  Publish to the server
  */
  
  if (!loopmq.connected()) {
    reconnect();                              // Try to reconnect if connection dropped
  }
  if (loopmq.connect(c, user, pass)){
 
   loopmq.publish(p,buffer);                     // Publish message to the server once only
     delay(1000);
  }
  
  loopmq.loop();                              // check if the network is connected and also if the client is up and running
}
